PUBLIC incrementSequence
_TEXT   SEGMENT

incrementSequence PROC
	mov edx,1
	xor eax,eax
$loop:
	repeat 256
	add rax,rdx
	endm
	sub rcx,1
	jnz $loop
	ret 0
incrementSequence ENDP
_TEXT   ENDS
END
