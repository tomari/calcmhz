	AREA    |.drectve|, DRECTVE
	EXPORT |incrementSequence|
	AREA    |.text$mn|, CODE, ARM64
|incrementSequence| PROC
	mov x1, x0
	eor x0, x0, x0
	add x2, x0, 1
|$loop|
	sub x1, x1, x2
	GBLA count
count SETA 0
	WHILE count < 64
count SETA count+1
	add x0, x0, x2
	WEND
	cbnz x1, |$loop|
	ret
	ENDP
	END
