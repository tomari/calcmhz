use clap::Parser;

/// Command line argument.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Minimum duration to run the test in seconds
    #[arg(short, long, default_value_t = humantime::Duration::from(calcmhz::REASONABLE_MIN_DURATION))]
    duration: humantime::Duration,
    /// Initial number of loops. Automatically increased to exceed the minimum duration
    #[arg(short, long, default_value_t = calcmhz::REASONABLE_INITIAL_LOOPS)]
    loops: u64,
}

fn main() {
    let args = Args::parse();
    match calcmhz::estimate_mhz(args.loops, args.duration.into()) {
        Ok(estimation) => {
            println!(
                "{} MHz, {} seconds, {} loops",
                estimation.mhz,
                estimation.elapsed.as_secs_f64(),
                estimation.loops
            );
        }
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    }
}
