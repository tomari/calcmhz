# calcmhz

Rust crate to estimate the CPU (processor) core frequency.
Available on [crates.io](https://crates.io/crates/calcmhz).

Estimates the CPU frequency by measuring the time it takes to run a sequence of
instructions in a dependency chain. The instruction sequence is written in assembly like this:

```asseembly
	addi a1,a1,1
	addi a1,a1,1
	addi a1,a1,1
	addi a1,a1,1
```

This method does not use performance counters (other than the wall clock), and
the estimated frequency is likely to be the boosted frequency if such a feature is available
on the system, subject to temperature and/or other conditions.

While this method of frequency estimation works most of the time, there are a number of known
example of microarchitectures where this measurement reports half the actual operating frequency
(e.g. IBM POWER7 and POWER8).
There is also an example of microarchitecture where this program reports double the frequency: Pentium 4. It runs ALUs at double the frequency.

This crate currently supports following platforms:

| ISA       | Linux   | macOS | Windows(MSVC) |
| --------- | ------- | ----- | ------------- |
| aarch64   | ☑       | ☑     | ☑             |
| arm       | ☑       |       |               |
| i586      | ☑       |       | ☑            |
| i686      | ☑       |       | ☑            |
| mips      | ☑       |       |               |
| mips64    | ☑       |       |               |
| powerpc   | ☑      |       |               |
| powerpc64 | ☑       |       |               |
| riscv64   | ☑       |       |               |
| s390x     | not yet |       |               |
| sparc     | ☑       |       |               |
| sparc64   | ☑       |       |               |
| x86_64    | ☑       | ☑     | ☑             |

## Example

```rust
let cpu_frequency_in_mhz = calcmhz::mhz().unwrap();
println!("{} MHz", cpu_frequency_in_mhz);
```

Full documentation is available in [docs.rs](https://docs.rs/calcmhz/).

## Command Line Interface

The file at the `example` directory in the repository provides with a simple command-line
interface to the functionality of this crate. You can install this cli using `cargo install calcmhz --examples --locked`.

```
calcmhz  --help
Command line interface for the calcmhz

Usage: calcmhz [OPTIONS]

Options:
  -d, --duration <DURATION>  Minimum duration to run the test in seconds [default: 500ms]
  -l, --loops <LOOPS>        Initial number of loops. Automatically increased to exceed the minimum duration [default: 10000]
  -h, --help                 Print help
  -V, --version              Print version
```

## Automated Docker Images

The aforementioned command line utility is automatically built in the CI pipeline and published in Docker container image format.
Visit [GitLab Container Registry](https://gitlab.com/tomari/calcmhz/container_registry/) for latest tags.

Usage example:

```sh
podman run -it --rm registry.gitlab.com/tomari/calcmhz:latest
```

## License

Copyright 2023 Hisanobu Tomari

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
