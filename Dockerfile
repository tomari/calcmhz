FROM rust:alpine AS builder

RUN apk add musl-dev

ADD . /app/

RUN cd /app/ && cargo build --release --examples

#
FROM scratch

COPY --from=builder /app/target/release/examples/calcmhz /
ENTRYPOINT ["/calcmhz"]
