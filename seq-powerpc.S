	.section        ".text"
	.globl incrementSequence
incrementSequence:
	li %r7,1
	mr %r6,%r3
	mr %r5,%r0
	mtctr %r4
	cmplw %r4,%r0
	bne loop
	bdnz next
loop:
	.rept 64
	add %r5,%r5,%r7
	.endr
	bdnz loop
next:
	sub %r3,%r3,%r7
	cmpwi %r3,-1
	bne loop
	srwi %r3,%r4,26
	slwi %r4,%r4,6
	rlwimi %r3,%r6,6,0,31-6
	blr
