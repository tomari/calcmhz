use std::collections::HashMap;

fn main() {
    let isa_substitutions = HashMap::from([
        ("i686", "i586"),
        ("armv7", "arm"),
        ("thumbv7neon", "arm"),
        ("armv5te", "arm"),
        ("powerpc64le", "powerpc64"),
        ("mips64el", "mips64"),
    ]);

    let target = std::env::var("TARGET").unwrap();
    let isa = target.split('-').next().unwrap();
    let seq_isa = if let Some(substitution) = isa_substitutions.get(&isa) {
        substitution
    } else {
        isa
    };
    let file = format!(
        "seq-{}.{}",
        seq_isa,
        if target.contains("msvc") { "asm" } else { "S" }
    );
    cc::Build::new().file(file).compile("calcmhz");
}
