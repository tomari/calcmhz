	.586P
PUBLIC _incrementSequence
_TEXT	SEGMENT

_x$ = 8
_incrementSequence	PROC
	xor ecx,ecx
	mov edx, DWORD PTR _x$[esp]
	xor eax,eax
	or ecx, DWORD PTR _x$[esp-4]
	push edi
	mov edi, 1
	jz $next
$loop:
	repeat 256
	add eax,edi
	endm
	sub ecx,1
	jnz $loop
$next:
	sub ecx,1
	sub edx,1
	jnc $loop
	pop edi
	mov eax, DWORD PTR _x$[esp-4]
	mov edx, DWORD PTR _x$[esp]
	shld edx,eax,8
	sal eax,8
	ret
_incrementSequence	ENDP
_TEXT	ENDS
END
